'use strict';

const assert = require('assert'),
    http = require('http'),
    util = require('util');

exports = module.exports = lastMile;
exports.HttpError = HttpError;
exports.HttpSuccess = HttpSuccess;

function HttpError(statusCode, errorOrMessage) {
    assert.strictEqual(typeof statusCode, 'number');
    assert(errorOrMessage instanceof Error || typeof errorOrMessage === 'string');

    Error.call(this);
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.status = statusCode;
    if (typeof errorOrMessage === 'string') {
        this.message = errorOrMessage;
        this.internalError = null;
        this.details = null;
    } else {
        this.message = errorOrMessage.message || 'Internal error';
        this.internalError = errorOrMessage;
        this.details = errorOrMessage.details || null;
    }
}
util.inherits(HttpError, Error);

function HttpSuccess(statusCode, body) {
    assert.strictEqual(typeof statusCode, 'number');

    this.status = statusCode;
    this.body = body || null;
}

function lastMile() {
    // eslint-disable-next-line no-unused-vars
    return function (error, req, res, next) {
        if (error instanceof HttpSuccess) {
            const success = error;
            if (success.body) {
                res.status(success.status).send(success.body);
            } else {
                res.status(success.status).end();
            }

            return;
        }

        let status, errorBody;

        if (error instanceof HttpError) {
            status = error.status || 500; // connect/express or our app

            // if the request took too long, assume it's a problem on the client
            if (error.timeout && status === 503) status = 408; // timeout() middleware

            errorBody = { status: http.STATUS_CODES[status], message: error.message };
            if (error.details) Object.assign(errorBody, error.details);
        } else if (error.code === 'EBADCSRFTOKEN') { // csurf middleware
            status = 403;
            errorBody = { status: http.STATUS_CODES[403], message: 'session has expired or form tampered with' };
        } else if (error.type === 'entity.parse.failed') { // body-parser (https://github.com/expressjs/body-parser/issues/122)
            status = 400;
            errorBody = { status: http.STATUS_CODES[400], message: 'Failed to parse body' };
        } else {  // some uncaught exception (or assert)
            status = 500;
            errorBody = { status: http.STATUS_CODES[500], message: error.message };
        }

        // dump the raw error to show a stacktrace
        if (status === 500) console.dir(error); // dir allows to also show the internalError object

        res.errorBody = errorBody; // set this for future middleware to know what we sent
        res.status(status).send(errorBody);
    };
}
